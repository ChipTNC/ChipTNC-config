#!/bin/sh

LEVEL=`grep level /var/log/supervisor/direwolf-stdout.log | tail -n 1 | awk '{ print $13; }'`

if [ ${LEVEL} -gt 100 ]; then
	echo "Reseting alsa defaults ${LEVEL}"
	alsactl restore
fi

FIX=`gpspipe -r -n 10 | grep GGA | awk -F ',' '{ print $7; }'`

if [ ${FIX} -gt 0 ]; then
	echo "FIX ${FIX}"
	LOST=`grep gps -i /var/log/supervisor/direwolf-stdout.log  | tail -n 1 | grep Lost`
	if [ "x${LOST}" != "x" ]; then
		echo "LOST gpsd fix restarting direwolf"
		# direwolf won't retry when it looses gpsd so restart it
		supervisorctl restart gpsd
		supervisorctl reetart direwolf
	fi
	#GPS=`grep -i gps /var/log/supervisor/direwolf-stdout.log`
	#echo "GPS $GPS"
fi
