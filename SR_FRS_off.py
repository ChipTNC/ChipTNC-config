#!/usr/bin/python

import serial
import time

read_val = ''
port = "/dev/ttyS0"
rev = 3
hiPower = False

def write_and_wait( cmd ) :
    ser.write( cmd )
    time.sleep(1)
    read_val = ser.read(size=64)
    if read_val is not '':
        print read_val
        print port
        return 1
    else :
        print( "No response to %s" % cmd )
        return 0

def set_gpio( gpio_num, dir, val = 0 ) :
    try :
        with open('/sys/class/gpio/export', 'a') as f:
            f.write( "%d\n" % gpio_num )
        time.sleep( 1 )
    except :
	    pass

    gpio_dir_fo = open('/sys/class/gpio/gpio%d/direction' % gpio_num, 'a')
    gpio_dir_fo.write( "%s\n" % dir )
    gpio_dir_fo.close()
    if( dir.startswith( 'out' ) ) :
        gpio_fo = open('/sys/class/gpio/gpio%d/value' % gpio_num, 'a')
        gpio_fo.write( "%d\n" % val )
        gpio_fo.close()

if rev == 1 :
    power_gpio = 414
    rf_en_gpio = 409
    ptt_gpio = 106
    rf_power_en_gpio = 0

if rev == 2 :
    squelch_gpio = 35
    ptt_gpio = 106
    rf_en_gpio = 107
    power_gpio = 108
    rf_power_en_gpio = 110
    set_gpio( rf_power_en_gpio, 'out', 1 )
    time.sleep( 5 )

if rev == 3 :
    squelch_gpio = 35
    ptt_gpio = 39
    rf_en_gpio = 37
    power_gpio = 38
    rf_power_en_gpio = 100
    set_gpio( rf_en_gpio, 'out', 1 )
    set_gpio( rf_power_en_gpio, 'out', 0 )
    time.sleep( 1 )

set_gpio( power_gpio, 'in' )

set_gpio( rf_en_gpio, 'out', 0 )

# PTT active low
set_gpio( ptt_gpio, 'out', 1 )
set_gpio( squelch_gpio, 'in' )
