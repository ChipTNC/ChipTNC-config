#!/bin/sh


if [ ! -d /sys/class/gpio/gpiochip413 ]; then
	echo "Configuring standard C.H.I.P."
	touch /tmp/I_am_a_CHIP
	systemctl stop serial-getty@ttyS0.service
else
	echo "Configuring CHIPPro"
	i2cset -y -f 0 0x34 0x82 0x82
	touch /tmp/I_am_a_PRO
	/usr/local/bin/fix-uart1
fi

/usr/local/bin/enable_gpio.sh
/usr/local/bin/disable_ptt.sh

/usr/local/bin/SR_FRS_config.py
#systemctl start gpsd

exit 0
