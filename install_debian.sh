#!/bin/sh

set -e

BUILD=0
CWD=`pwd`
ETC_FILES="direwolf.conf debian/hostapd.conf"
BIN_FILES="SR_FRS_config.py disable_ptt.sh  enable_ptt.sh enable_gpio.sh"

for F in ${ETC_FILES}; do
    cp $F /etc
done

for F in ${BIN_FILES}; do
    cp $F /usr/local/bin
done

sudo dpkg --configure -a
sudo apt-get install -y python python-dev
sudo apt-get install -y supervisor gpsd gpsd-clients python-webpy python-alsaaudio
if ${BUILD}; then
	sudo apt-get install -y make gcc vim vim-scriptS libgps-dev
fi
sudo apt-get install -y vim vim-scripts
sudo apt-get install -y lighttpd-mod-magnet python-flup fcgiwrap 
sudo apt-get clean

cp debian/interfaces /etc/network/interfaces
sudo mkdir -p /etc/dnsmasq.d/
cp debian/access_point.conf /etc/dnsmasq.d/
cp debian/lighttpd.conf /etc/lighttpd/
cp gpsd /etc/default
cd /tmp && wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py
 
if ${BUILD}; then
	cd /tmp
	git clone --depth 1 https://github.com/nytowl/direwolf.git
	cd direwolf
	make
	sudo make install
else
	cd /tmp
	wget https://akkea.ca/files/direwolf
	sudo mkdir -p /usr/local/bin
	sudo mv direwolf /usr/local/bin
	sudo chmod a+x /usr/loca/bin/direwolf
fi

if ${BUILD}; then
	cd /tmp
	wget http://thelifeofkenneth.com/aprx/release/aprx-2.9.0.tar.gz
	tar -zxf aprx-2.9.0.tar.gz
	cd aprx-2.9.0
	./configure --prefix=/usr
	make
	sudo make install
else
	wget https://akkea.ca/files/aprx_2.9.0-1_armhf.deb
	dpkg -i aprx_2.9.0-1_armhf.deb
fi

cd /tmp
sudo pip uninstall -y pyserial || true
sudo pip install aprs==6.5.0 kiss==6.5.0b2
sudo pip install pyserial gps3 web.py smbus2

sudo usermod -a -G audio www-data
sudo usermod -a -G i2c www-data
sudo groupadd supervisor || true
sudo usermod -a -G supervisor www-data

cd ${CWD}
sudo cp messaged.conf /etc/supervisor/conf.d/
sudo cp debian/aprx.conf /etc/supervisor/conf.d/
sudo cp debian/direwolf-supervisord.conf /etc/supervisor/conf.d/direwolf.conf
sudo cp debian/supervisord.conf /etc/supervisor/
sudo systemctl restart supervisor

cp SR_FRS_config.py /usr/local/bin
cp config-tnc.sh /usr/local/bin
cp restart_aprs.sh /usr/local/bin/restart_aprs.sh
sudo chown root:root /usr/local/bin/restart_aprs.sh
sudo chmod a+x /usr/local/bin/restart_aprs.sh
sudo chmod u+s /usr/local/bin/restart_aprs.sh

sudo cp debian/chip-tnc.service /lib/systemd/system/
sudo systemctl enable chip-tnc.service

sudo mkdir -p /var/lib/GMapCatcher/tiles
sudo cp debian/back_fill_tiles /etc/cron.d

cd /var/www
git clone https://gitlab.com/ChipTNC/ChipTNC-www.git html
sudo chown -R chip:chip /var/www/html
