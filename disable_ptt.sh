#!/bin/sh

if [ ! -d /sys/class/gpio/gpiochip413 ]; then
	sudo sh -c 'echo 0 > /sys/class/gpio/gpio413/value'
	# active low for RF module
	sudo sh -c 'echo 1 > /sys/class/gpio/gpio412/value'
	# active high for RF audio plug
	sudo sh -c 'echo 0 > /sys/class/gpio/gpio412/value'
else
	# active high for RF audio plug
	sudo sh -c 'echo 0 > /sys/class/gpio/gpio40/value'
	sudo sh -c 'echo 0 > /sys/class/gpio/gpio41/value'
	# active low for RF module
	sudo sh -c 'echo 1 > /sys/class/gpio/gpio39/value'
fi
