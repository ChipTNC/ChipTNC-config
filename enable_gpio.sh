#!/bin/sh

if [ ! -d /sys/class/gpio/gpiochip413 ]; then
	REV=2
else
	REV=3
fi

export_and_set () {
	GPIO=$1
	DIR=$2

	if [ ${DIR} == "out" ]; then
		VAL=$3
	fi

	echo "Setting up gpio ${GPIO} dir ${DIR} val ${VAL}"
    
	if [ ! -e /sys/class/gpio/gpio${GPIO} ]; then
 		echo ${GPIO} > /sys/class/gpio/export
	fi
    	echo ${DIR} > /sys/class/gpio/gpio${GPIO}/direction
	if [ ${DIR} == "out" ]; then
    		echo ${VAL} > /sys/class/gpio/gpio${GPIO}/value
	fi
}

if [ ${REV} -eq 2 ]; then
    # PTT S
    export_and_set 102 out 0

    # PTT M
    export_and_set 102 out 0

    # PTT RF
    export_and_set 106 out 0

    # RF_EN
    export_and_set 107 out 0

    # Power level
    export_and_set 108 in

    # EXP RST
    export_and_set 109 out 1
    
    # RF power
    export_and_set 110 out 1
fi

if [ ${REV} -eq 3 ]; then
    # PTT S
    export_and_set 41 out 0

    # PTT M
    export_and_set 40 out 0

    # PTT RF
    export_and_set 39 out 0

    # RF_EN
    export_and_set 37 out 0

    # Power level
    export_and_set 38 in

    # EXP RST
    export_and_set 101 out 1
    
    # RF power
    export_and_set 100 out 1
fi

