#!/bin/sh

PERCENT=`/usr/sbin/i2cget -y -f 0 0x34 0xb9`
STATUS=`/usr/sbin/i2cget -y -f 0 0x34 0x00`

printf "Status 0x%x battery percentage %i\n" ${STATUS} ${PERCENT}

