#!/usr/bin/python
import aprs
import kiss
import sys
sys.path.append('/var/www/html')
import config as cfg
import getopt
import re
import time
from types import *
import telnetlib

#ishost = "rotate.aprs2.net"
ishost = "noam.aprs2.net"
isport = 14580


def publish_is_message(msg, destination, src, passwd):
    print "publish {0} to {1} from {2}".format(msg,destination,src)
    tn = telnetlib.Telnet(ishost, isport)
    tn.write("user " + src + " pass " + passwd + " vers Python/APRS 0.1\n")
    out="{0}>APDW14,TCPIP*::{1:<9}:{2}\r\n".format(src, destination, msg)
    tn.write(out)
    tn.close()

def publish_message(msg, destination, ack_rx=0):
    print "publish {0} to {1}".format(msg,destination)
    config = cfg.getConfigValues( [ "MYCALL" ] )
    frame = aprs.Frame()
    frame.source = aprs.Callsign(config['MYCALL'])
    frame.destination = aprs.Callsign('APDW13')
    frame.path = [aprs.Callsign('WIDE1-1'),aprs.Callsign('WIDE2-1')]
    frame.text = ':%-9s:' % destination
    frame.text += msg
    msgId = 0
    if ack_rx :
        msgId += 1
    frame.text += "{%-5d" % msgId

    ki = kiss.TCPKISS(host='localhost', port=8001)
    ki.start()
    ki.write(frame.encode_kiss())
    ki.stop()

def usage():
    print("Usage: -m <message> -d <destination callsign> [-s <src> -p <passwd>]")

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "m:d:p:s:h", ["message","destination","source","password","help"])
    except getopt.GetoptError as err:
        usage()
        sys.exit(2)

    message = None
    dest = None
    transport="rf"
    for o, a in opts:
        if o == '-m':
            message = a
        if o == '-d':
            dest = a
        if o == '-p':
            passwd=a
            transport="is"
        if o == '-s':
            src=a
            transport="is"

    if message==None or dest==None:
        usage()
        sys.exit()

    if transport=="rf":
        publish_message(message, dest)
    else:
        publish_is_message(message, dest, src, passwd)

if __name__ == "__main__":
    main()
