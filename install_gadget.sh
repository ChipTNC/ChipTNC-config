#!/bin/sh

BUILD=0
CWD=`pwd`
ETC_FILES="direwolf.conf"
BIN_FILES="SR_FRS_config.py disable_ptt.sh enable_ptt.sh enable_gpio.sh"

set -e 

for F in ${ETC_FILES}; do
    sudo cp $F /data/etc
done

if [ ! -e /usr/local/bin ]; then
    sudo mkdir -p /usr/local/bin
fi

for F in ${BIN_FILES}; do
    sudo cp $F /usr/local/bin
done

sudo cp -f gadget/lighttpd.conf /data/etc/lighttpd/
 
cd /tmp
wget https://akkea.ca/files/fix-uart1
sudo mv fix-uart1 /usr/local/bin

if ${BUILD}; then
	cd /tmp
	wget http://thelifeofkenneth.com/aprx/release/aprx-2.9.0.tar.gz
	tar -zxf aprx-2.9.0.tar.gz
	cd aprx-2.9.0
	./configure --prefix=/usr/local
	make
	sudo make install
else
	wget https://akkea.ca/files/aprx
	sudo mv aprx /usr/local/bin
fi

sudo chmod +x /usr/local/bin/*

cd ${CWD}
sudo mkdir -p /data/etc/supervisor.d/conf.d/
sudo cp messaged.conf /data/etc/supervisor.d/conf.d/
sudo cp gadget/aprx.conf /data/etc/supervisor.d/conf.d/
sudo cp gadget/gpsd.conf /data/etc/supervisor.d/conf.d/
sudo cp gadget/direwolf-supervisord.conf /data/etc/supervisor.d/conf.d/direwolf.conf
sudo cp gadget/supervisord.conf /data/etc/
sudo cp gadget/S51direwolf /data/etc/init.d/
#sudo supervisorctl reload

sudo cp blink.conf /data/etc/supervisor.d/conf.d/
sudo cp low_battery_shutdown /usr/local/bin/
sudo chmod 755 /usr/local/bin/low_battery_shutdown
sudo cp send_message.py /usr/local/bin/
sudo chmod 755 /usr/local/bin/send_message.py
sudo cp SR_FRS_config.py /usr/local/bin
sudo cp config-tnc.sh /usr/local/bin
sudo cp restart_aprs.sh /usr/local/bin/restart_aprs.sh
sudo cp gadget/check_direwolf.sh /usr/local/bin/
sudo chown root:root /usr/local/bin/restart_aprs.sh
sudo chmod a+x /usr/local/bin/restart_aprs.sh
sudo chmod u+s /usr/local/bin/restart_aprs.sh

sudo cp asound.state /var/lib/alsa/asound.state

sudo mkdir -p /data/GMapCatcher/tiles
sudo cp gadget/back_fill_tiles /etc/cron.d
sudo cp gadget/check_direwolf /etc/cron.d

if [ ! -e /var/www/html ]; then
	sudo chmod a+rw /var/www
	cd /var/www
	git clone https://gitlab.com/ChipTNC/ChipTNC-www.git html
fi

# 
# 
cd
sudo mkdir -p /data/etc/chip/.ssh
sudo chown -R chip:chip /data/etc/chip
sudo mount -o rw,remount /
sudo ex +"%s,/home/chip,/data/etc/chip" -cwq /etc/passwd
sudo mount -o ro,remount /
if [ -e id_rsa ]; then
	cp id_rsa id_rsa.pub /data/etc/chip/.ssh/
fi

cd /tmp

git clone https://github.com/hpeyerl/blink.git
sudo cp blink/blink.sh /usr/local/bin/blink.sh
sudo chmod +x /usr/local/bin/blink.sh
sudo cp blink/blink.cfg /data/etc/blink.cfg
sudo supervisorctl reload
