#!/usr/bin/python

import serial
import time
import os

read_val = ''
port = "/dev/ttyS0"

try:
    os.stat("/sys/class/gpio/gpiochip413")
    print( "Running on a C.H.I.P. Pro" )
    rev = 3
except:
    print( "Running on a standard C.H.I.P." )
    rev = 2
    pass

def write_and_wait( cmd ) :
    ser.write( cmd )
    #time.sleep(1)
    read_val = ser.read(size=64)
    if read_val is not '':
        print read_val
        print port
        return 1
    else :
        print( "No response to %s" % cmd )
        return 0

def set_gpio( gpio_num, dir, val = 0 ) :
    try :
        with open('/sys/class/gpio/export', 'a') as f:
            f.write( "%d\n" % gpio_num )
        time.sleep( 1 )
    except :
	    pass

    gpio_dir_fo = open('/sys/class/gpio/gpio%d/direction' % gpio_num, 'a')
    gpio_dir_fo.write( "%s\n" % dir )
    gpio_dir_fo.close()
    if( dir.startswith( 'out' ) ) :
        gpio_fo = open('/sys/class/gpio/gpio%d/value' % gpio_num, 'a')
        gpio_fo.write( "%d\n" % val )
        gpio_fo.close()

if rev == 2 :
    squelch_gpio = 35
    ptt_gpio = 106
    rf_en_gpio = 107
    power_gpio = 108
    rf_power_en_gpio = 110

if rev == 3 :
    squelch_gpio = 35
    ptt_gpio = 39
    rf_en_gpio = 37
    power_gpio = 38
    rf_power_en_gpio = 100

set_gpio( rf_power_en_gpio, 'out', 1 )
time.sleep( 1 )

set_gpio( power_gpio, 'in' )

#set_gpio( power_gpio, 'out', 0 )
# does this reset it ?
set_gpio( rf_en_gpio, 'out', 0 )
time.sleep( 1 )
set_gpio( rf_en_gpio, 'out', 1 )
# PTT active low
set_gpio( ptt_gpio, 'out', 1 )
# squelch active low
#set_gpio( squelch_gpio, 'in' )

# RF module takes 70 ms to boot
time.sleep( 1 )

# bauds=[ 1200, 2400, 4800, 9600, 14400, 38400, 57600, 115200 ]
bauds =[9600]

for baud in bauds :
    print( "Trying baud rate %d\n" % baud )
    ser = serial.Serial(port, baud, timeout=1)
    ser.close()
    ser.open()

    write_and_wait( "AT+DMOCONNECT\r\n" )
    write_and_wait( "AT+DMOCONNECT\r\n" )
    if write_and_wait( "AT+DMOCONNECT\r\n" ) :
        break

write_and_wait( "AT+DMOSETGROUP=0,144.3900,144.3900,0,0,0,0\r\n" )
write_and_wait( "AT+DMOVERQ\r\n" )
write_and_wait( "AT+DMOSETVOLUME=1\r\n" )
write_and_wait( "AT+DMOAUTOPOWCONTR=1\r\n" )
if not write_and_wait( "AT+DMOSETVOX=0\r\n" ) :
    set_gpio( rf_en_gpio, 'out', 0 )
    set_gpio( ptt_gpio, 'out', 0 )
    set_gpio( rf_power_en_gpio, 'out', 0 )

ser.close()
